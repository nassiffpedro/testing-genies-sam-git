CREATE TABLE app_start (
   logId VARCHAR(100) PRIMARY KEY,
   userId VARCHAR(100),
   epoch_time BIGINT,
   local_date_time TIMESTAMP WITH TIME ZONE,
   message VARCHAR(100),
   last_known_user_id VARCHAR(100),
   quit_epoch_time BIGINT,
   quit_local_date_time TIMESTAMP WITH TIME ZONE,
   session_time_seconds REAL,
   last_page_name VARCHAR(100)
);

CREATE VIEW view_app_start_per_day AS
SELECT CAST(TIMESTAMP 'epoch' + epoch_time * INTERVAL '1 SECOND' AS DATE), COUNT(*) AS app_starts FROM app_start GROUP BY date ORDER BY date;