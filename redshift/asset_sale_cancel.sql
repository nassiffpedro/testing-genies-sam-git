CREATE TABLE asset_sale_cancel (
  logid VARCHAR(100) PRIMARY KEY,
  userid VARCHAR(100),
  timestamp BIGINT,
  guid VARCHAR(100),
  title VARCHAR(100),
  description VARCHAR(1000),
  is_verified BOOL,
  rarity_label VARCHAR(100),
  price REAL,
  is_free_item BOOL,
  is_sold_out BOOL,
  is_own BOOL,
  has_timer BOOL,
  is_on_marketplace BOOL,
  rarity INT,
  expiration TIMESTAMP,
  preview_sprite_texture SUPER,
  show_headers BOOL,
  upper_label_type INT,
  lower_label_type INT
);