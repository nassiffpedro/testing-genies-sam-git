CREATE TABLE avatar_save (
  logId VARCHAR(100) PRIMARY KEY,
  userId VARCHAR(100),
  timestamp BIGINT,
  eye_color VARCHAR(50),
  skin_color VARCHAR(50),
  body_type_label VARCHAR(50),
  facial_hair_color VARCHAR(50),
  face_blend_shapes SUPER,
  clothes SUPER,
  hair_color VARCHAR(50),
  eyebrow_color VARCHAR(50)
);