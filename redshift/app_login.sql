CREATE TABLE app_login (
  logId VARCHAR(100) PRIMARY KEY,
  userId VARCHAR(100),
  epoch_time BIGINT,
  local_date_time TIMESTAMP WITH TIME ZONE,
  message VARCHAR(100),
  quit_epoch_time BIGINT,
  quit_local_date_time TIMESTAMP WITH TIME ZONE,
  session_time_seconds REAL,
  last_page_name VARCHAR(100)
);