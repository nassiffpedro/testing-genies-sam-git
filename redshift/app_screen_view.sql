CREATE TABLE app_screen_view (
  logId VARCHAR(100) PRIMARY KEY,
  userId VARCHAR(100),
  timestamp BIGINT,
  page_name VARCHAR(100),
  last_known_user_id VARCHAR(100)
);
