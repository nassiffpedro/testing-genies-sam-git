function appScreenView(item) {
  return {
    Data: JSON.stringify({
      logid: item.logId.S,
      userid: item.userId.S,
      timestamp: parseInt(item.timestamp.N / 1000),
      page_name: item.value.M.page_name.S,
      last_known_user_id: item.value.M.last_known_user_id.S
    }),
  };
}

module.exports = appScreenView;
