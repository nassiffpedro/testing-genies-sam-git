const AWS = require('aws-sdk');
const appLogin = require('./app_login');
const appScreenView = require('./app_screen_view');
const appStart = require('./app_start');
const assetSale = require('./asset_sale');
const assetSaleCancel = require('./asset_sale_cancel');
const assetTryOn = require('./asset_try_on');
const avatarSave = require('./avatar_save');
const buttonClick = require('./button_click');
const firehoseClient = new AWS.Firehose();
const deliveryStreamName = {
  'Analytics.AppLogin': 'genies_analytics_app_login',
  'Analytics.AppScreenView': 'genies_analytics_app_screen_view',
  'Analytics.AppStart': 'genies_analytics_app_start',
  'Analytics.AssetSale': 'genies_analytics_asset_sale',
  'Analytics.AssetSaleCancel': 'genies_analytics_asset_sale_cancel',
  'Analytics.AssetTryOn': 'genies_analytics_asset_try_on',
  'Analytics.AvatarSave': 'genies_analytics_avatar_save',
  'Analytics.ButtonClick': 'genies_analytics_button_click',
};
const records = {
  genies_analytics_app_login: [],
  genies_analytics_app_screen_view: [],
  genies_analytics_app_start: [],
  genies_analytics_asset_sale: [],
  genies_analytics_asset_sale_cancel: [],
  genies_analytics_asset_try_on: [],
  genies_analytics_avatar_save: [],
  genies_analytics_button_click: [],
};

exports.handler = async (event, context) => {
  for (let record of event.Records) {
    try {
      // If the record comes from kinesis data stream (for existing data), is decoded from base64
      if (record.eventName === 'aws:kinesis:record') {
        record = JSON.parse(Buffer.from(record.kinesis.data, 'base64').toString('utf-8'));
      }
      if (record.eventName === 'INSERT') {
        const DeliveryStreamName = deliveryStreamName[record.dynamodb.NewImage.name.S];
        let processedRecord;
        switch (DeliveryStreamName) {
          case 'genies_analytics_app_login':
            processedRecord = appLogin(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_app_screen_view':
            processedRecord = appScreenView(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_app_start':
            processedRecord = appStart(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_asset_sale':
            processedRecord = assetSale(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_asset_sale_cancel':
            processedRecord = assetSaleCancel(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_asset_try_on':
            processedRecord = assetTryOn(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_avatar_save':
            processedRecord = avatarSave(record.dynamodb.NewImage);
            break;
          case 'genies_analytics_button_click':
            processedRecord = buttonClick(record.dynamodb.NewImage);
            break;
        }
        if (processedRecord) records[DeliveryStreamName].push(processedRecord);
      }
    } catch (err) {
      console.log(err);
    }
  }

  for (const DeliveryStreamName of Object.keys(records)) {
    if (records[DeliveryStreamName].length) {
      const params = {
        Records: records[DeliveryStreamName],
        DeliveryStreamName,
      };

      firehoseClient.putRecordBatch(params, function (err, data) {
        if (err) console.log(err, err.stack);
        else console.log(data);
      });

      // cleans array, otherwise the next invocation will have the previous events
      records[DeliveryStreamName] = [];
    }
  }
  return `Successfully processed ${event.Records.length} records.`;
};
