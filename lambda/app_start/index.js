function appStart(item) {
  const local_date_time = item.value.M.local_date_time.S;
  const quit_local_date_time = item.value.M.last_session_quit_data.M.local_date_time.S;
  return {
    Data: JSON.stringify({
      logid: item.logId.S,
      userid: item.userId.S,
      epoch_time: parseInt(item.value.M.epoch_time.S),
      local_date_time: local_date_time.slice(0, 10) + ' ' + local_date_time.slice(11,) + local_date_time.slice(27, 30),
      message: item.value.M.message.S,
      last_known_user_id: item.value.M.last_known_user_id.S,
      quit_epoch_time: parseInt(item.value.M.last_session_quit_data.M.epoch_time.S),
      quit_local_date_time: quit_local_date_time.slice(0, 10) + ' '+ quit_local_date_time.slice(11, 19) + quit_local_date_time.slice(27, 30),
      session_time_seconds: parseFloat(item.value.M.last_session_quit_data.M.session_time_seconds.S),
      last_page_name: item.value.M.last_session_quit_data.M.last_page_name.S,
    }),
  };
}

module.exports = appStart;