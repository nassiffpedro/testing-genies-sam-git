function assetSale(item) {
  return {
    Data: JSON.stringify({
      logid: item.logId.S,
      userid: item.userId.S,
      timestamp: parseInt(item.timestamp.N / 1000),
      guid: item.value.M.Guid.S,
      title: item.value.M.Title.S,
      description: item.value.M.Description.S,
      is_verified: item.value.M.IsVerified.BOOL,
      rarity_label: item.value.M.RarityLabel.S,
      price: item.value.M.Price.N,
      is_free_item: item.value.M.IsFreeItem.BOOL,
      is_sold_out: item.value.M.IsSoldOut.BOOL,
      is_own: item.value.M.IsOwn.BOOL,
      has_timer: item.value.M.HasTimer.BOOL,
      is_on_marketplace: item.value.M.IsOnMarketplace.BOOL,
      rarity: item.value.M.Rarity.N,
      expiration: item.value.M.Expiration.S.slice(0, 10) + ' ' + item.value.M.Expiration.S.slice(11, 19),
      preview_sprite_texture: item.value.M.previewSpriteTexture.M,
      show_headers: item.value.M.ShowHeaders.BOOL,
      upper_label_type: item.value.M.UpperLabelType.N,
      lower_label_type: item.value.M.LowerLabelType.N,
    }),
  };
}

module.exports = assetSale;
