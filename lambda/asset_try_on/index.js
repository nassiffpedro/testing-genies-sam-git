function assetTryOn(item) {
  return {
    Data: JSON.stringify({
      logid: item.logId.S,
      userid: item.userId.S,
      timestamp: parseInt(item.timestamp.N / 1000),
      assets: item.value.M.assets.L
    }),
  };
}

module.exports = assetTryOn;
