function buttonClick(item) {
  return {
    Data: JSON.stringify({
      logid: item.logId.S,
      userid: item.userId.S,
    }),
  };
}

module.exports = buttonClick;
