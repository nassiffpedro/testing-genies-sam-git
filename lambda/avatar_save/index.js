function avatarSave(item) {
  return {
    Data: JSON.stringify({
      logid: item.logId.S,
      userid: item.userId.S,
      timestamp: parseInt(item.timestamp.N / 1000),
      eye_color: item.value.M.EyeColor.S,
      skin_color: item.value.M.SkinColor.S,
      body_type_label: item.value.M.BodyTypeLabel.S,
      facial_hair_color: item.value.M.FacialHairColor.S,
      face_blendshapes: item.value.M.FaceBlendshapes.L,
      clothes: item.value.M.Clothes.L,
      hair_color: item.value.M.HairColor.S,
      eyebrow_color: item.value.M.EyebrowColor.S,
    }),
  };
}

module.exports = avatarSave;
