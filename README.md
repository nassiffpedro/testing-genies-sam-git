# genies-analytics

# 1. crear tabla en redshift

# 2. crear kinesis

# 3. crear funcion lambda

# 4. deploy en lambda

# 5. cargar el evento

# 6. correr el script py

# 7. controlar que impacte

# 8. git branch <evento> -> push -> pr

# Scripts to run

##Cloudformation:


Deploy:

    aws cloudformation create-stack --stack-name genies-analytics --template-body file://cloudformation-stack-lambda-test.json --capabilities CAPABILITY_NAMED_IAM
    aws cloudformation update-stack --stack-name genies-analytics --template-body file://cloudformation-stack.json --capabilities CAPABILITY_NAMED_IAM

Rollback: (Complete)

    aws s3 rm s3://genies-analytics-deployment --recursive
    aws s3 rm s3://genies-analytics-data --recursive
    aws cloudformation delete-stack --stack-name genies-analytics


Copy to lambda .zip files:

    zip ../lambda.zip .    (Be located into lambda folder to exlude parent folder creation)
    aws lambda update-function-code --function-name genies-analytics-events-processor --zip-file fileb://lambda.zip