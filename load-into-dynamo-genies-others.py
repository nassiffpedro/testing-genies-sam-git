import os, sys, time, decimal
from decimal import *
import boto3
import json
dynamodb = boto3.resource('dynamodb', region_name='sa-east-1')
table = dynamodb.Table('genies-others')


def loadfile(infile):
    jsonobj = json.load(open(infile))
    insertedRows = 1
    for event in jsonobj:
        insertedRows += 1
        value = ''
        try:
            value = json.loads(event['value'])
            response = table.put_item(
            Item={
                    'logId': event['logId'],
                    'name': event['name'],
                    'value': value,
                    'userId': event['userId'],
                    'timestamp': event['timestamp']
                }
            )
        except ValueError as e:
            print (e)
        if (insertedRows % 10) == 0:
            print ("%d rows inserted" % (insertedRows))

if __name__ == '__main__':
    filename = sys.argv[1]
    if os.path.exists(filename):
        # file exists, continue
        loadfile(filename)
    else:
        print ('Please enter a valid filename')