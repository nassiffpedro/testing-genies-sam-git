import json
import boto3
import boto3.dynamodb.types

# Note: AWS credentials should be passed as environment variables or through IAM roles.
dynamodb = boto3.resource('dynamodb', region_name="sa-east-1")
kinesis = boto3.client('kinesis', region_name="sa-east-1")

# Load the DynamoDB table.
ddb_table_name = "genies-app"
ddb_partition_key = "logId"
ks_stream_name = "genies-analytics-kinesis-data-stream"
table = dynamodb.Table(ddb_table_name)

# Get the primary keys.
ddb_keys_name = [a['AttributeName'] for a in table.attribute_definitions]


# Scan operations are limited to 1 MB at a time.
response = {}
record_count = 0

# Search the last evaluated key in the file if it exists
file_name = ddb_table_name+"-last_evaluated_key.txt"
try:
    last_evaluated_key_file = open(file_name, "r")
    last_evaluated_key_lines = last_evaluated_key_file.read().splitlines()
    if last_evaluated_key_lines:
        last_evaluated_key = last_evaluated_key_lines[-1]
        response['LastEvaluatedKey'] = json.loads(last_evaluated_key)
    last_evaluated_key_file.close()
except:
    print('Failed to open ' + file_name)
    print('The table will be readed from the start')
last_evaluated_key_file = open(file_name, "a+")

# Iterate until all records have been scanned.
while True:
    if not response:
        # Scan from the start.
        response = table.scan()
    else:
        # Scan from where you stopped previously.
        response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])

    for i in response["Items"]:
        record_count += 1
        # Get a dict of primary key(s).
        ddb_keys = {k: i[k] for k in i if k in ddb_keys_name}
        # Serialize Python Dictionaries into DynamoDB notation.
        ddb_data = boto3.dynamodb.types.TypeSerializer().serialize(i)["M"]
        ddb_keys = boto3.dynamodb.types.TypeSerializer().serialize(ddb_keys)["M"]
        # The record must contain "Keys" and "NewImage" attributes to be similar to a DynamoDB Streams record.
        record = {"eventName": "INSERT", "dynamodb": {"Keys": ddb_keys, "NewImage": ddb_data}}
        # Convert the record to JSON.
        record = json.dumps(record)
        # Push the record to Amazon Kinesis.
        res = kinesis.put_record(
            StreamName=ks_stream_name,
            Data=record,
            PartitionKey=i[ddb_partition_key]
        )
        if (record_count % 10) == 0:
            print ("%d rows inserted" % (record_count))

    # Saves the key of the last readed record
    last_evaluated_key_file.write(json.dumps(response['LastEvaluatedKey'])+"\n")

    # Stop the loop if no additional records are available.
    if 'LastEvaluatedKey' not in response:
        break

